package com.example.testloybot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class TestLoyBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestLoyBotApplication.class, args);
    }

}
