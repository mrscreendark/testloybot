package com.example.testloybot.model;

import java.util.UUID;


public record City (
         UUID id,
         String name){

}
