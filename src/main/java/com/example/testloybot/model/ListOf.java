package com.example.testloybot.model;

import java.util.List;

public record ListOf<T> (
       int totalItems,
       List<T> items
){

}
