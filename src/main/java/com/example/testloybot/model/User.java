package com.example.testloybot.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {


    private Long id;


    private String name;

    private Long chatId;

    private Long permissions;

//    private State state;

    private String userName;
}