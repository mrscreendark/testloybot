package com.example.testloybot.message;

import com.example.testloybot.botcustom.Buttons;
import com.example.testloybot.config.BotConfig;
import com.example.testloybot.httpconnect.SendRequest;
import com.example.testloybot.model.City;
import com.example.testloybot.model.ListOf;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
@AllArgsConstructor
public class TelegramBot extends TelegramLongPollingBot {

    private final BotConfig botConfig;
    private final SendRequest sendRequest;


    @Override
    public String getBotUsername() {
        return botConfig.getBotName();
    }

    @Override
    public String getBotToken() {
        return botConfig.getToken();
    }

    @Override
    public void onUpdateReceived(Update update) {

        if(update.hasMessage() && update.getMessage().hasText()){
            String messageText = checkSignature(update.getMessage().getText());
            long chatId = update.getMessage().getChatId();
//            Message message = update.getMessage();

            switch (messageText){
                case "/start":
                    startCommandReceived(chatId, update.getMessage().getChat().getFirstName());
                    break;

                case "/getList":
                    ListOf<City> messageSend = sendRequest.getJopa("Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTcwMjM4MDY2MCwicm9sZSI6IkFETUlOIiwiZmlyc3ROYW1lIjoi0JDQu9C10LrRgdC10LkifQ.2iccrGg7S4b0IffdwDlRq6Gz2yiBRPhsGwh5s59x4eQvjfOIxBRrcpb414j7g8nSTc9-vUknMclTMi279CK1zQ");
                    System.out.println("Receive mes ----->>>>>>  " + messageSend.toString());
                    sendMessage(chatId, messageSend.toString());
                    break;

                default: sendMessage(chatId, "Ты ошибся братан");
            }
        }

    }

    private void startCommandReceived(Long chatId, String name) {
        String answer = "Hi, you can get list of cities";
        sendMessage(chatId, answer);
    }

    private void sendMessage(Long chatId, String textToSend){
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setReplyMarkup(Buttons.replyKeyboardMarkup());

        sendMessage.setChatId(String.valueOf(chatId));
        sendMessage.setText(textToSend);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            System.out.println("Exception sending ---------------------->>>>>>>>>>>>");
        }
    }

    private String checkSignature(String text) {
        if (text.equals("Список городов")){
            return "/getList";
        } else if (text.equals("Старт / Перезагрузка") || text.equals("/start")){
            return "/start";
        }
        return "ex";
    }

}
