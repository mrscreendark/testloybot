package com.example.testloybot.httpconnect;


import com.example.testloybot.model.City;
import com.example.testloybot.model.ListOf;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(
        value = "paymentClient",
        url = "http://90.156.206.55:8079")
public interface SendRequest {

    @GetMapping(value = "/loyalty-portal/api/v1/persons/cities",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    ListOf<City> getJopa(@RequestHeader("Authorization") String bearerSign);


}